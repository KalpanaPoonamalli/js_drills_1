function problem2(inventory, val){
    if (inventory.length == 0 || !Array.isArray(inventory)){
        return inventory = [];
    } if (val == null || typeof val != 'number') {
        return [];
    }

    return inventory[val];
}

module.exports = problem2 ;