function problem6(inventory){
    if (inventory.length == 0 || !Array.isArray(inventory)){
        return inventory = [];
    }

    let array = [];
    let obj = 0;
    for(let index=0; index<inventory.length; index++){
        if (inventory[index].car_make === 'BMW' || inventory[index].car_make === 'Audi'){
            array[obj] = inventory[index].car_make
            obj++
        }
    }
    return array;
}

module.exports = problem6;