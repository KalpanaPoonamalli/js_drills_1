let inventory = require('../inventory.cjs');
let problem1 = require('../problem1.cjs');

const result = problem1(inventory, 33);

let answer;

if (result){
   answer = (`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}else {
    answer  = 'Car Not Found';
}

console.log(answer);
