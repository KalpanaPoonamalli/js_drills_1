let inventory = require('../inventory.cjs')
let problem2 = require('../problem2.cjs')

const result = problem2(inventory, inventory.length-1);

let answer;

if (result) {
    answer = (`Last Car is a ${result.car_make} ${result.car_model}`);
}else {
    answer = "Car Not Found";
}

console.log(answer);
