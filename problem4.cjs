function problem4(inventory){
    if (inventory.length == 0 || !Array.isArray(inventory)){
        return inventory = [];
    }

    let array = [];
    for (let index=0; index< inventory.length; index++){
        array[index] = inventory[index].car_year;
    }
    return array;
}

module.exports = problem4;