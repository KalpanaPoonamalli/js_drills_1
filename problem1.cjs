function problem1(inventory, val) {
    
    if(inventory=='' || !Array.isArray(inventory)){
        return inventory = [];
    }if (val==null || typeof val != 'number'){
        return [];
    }

    for (let index=0; index<inventory.length; index++){
        if(inventory[index].id == val){
            return inventory[index];
        }
    }
}


module.exports = problem1 ;