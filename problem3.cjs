function problem3(inventory){
    if (inventory.length == 0 || !Array.isArray(inventory)){
        return inventory = [];
    }

    inventory.sort((a,b) => {
        if(a.car_model > b.car_model){
            return 1;
        } else if (a.car_model < b.car_model){
            return -1;
        }else {
            return 0;
        }
    });

    let array = [];
    for(let index=0; index<inventory.length; index++){
        array[index] = inventory[index].car_model
    }
    return array;
}

module.exports = problem3;