function problem5(inventory){
    if (inventory.length == 0 || !Array.isArray(inventory)){
        return inventory = [];
    }

    let array= [];
    let obj = 0 ;
    for(let index=0; index<inventory.length; index++){
        if (inventory[index].car_year < 2000){
            array[obj] = inventory[index].car_year; 
            obj++;
        }
    }
    return array;
} 


module.exports = problem5 ;